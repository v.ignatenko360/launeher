import './index.sass';
import './index.pug';

import './head-slider/head-slider.sass';
import './head-slider/index';

import './working/working.sass';

import './ideas/ideas.sass';

import './projects/projects.sass';
import './projects/index';

import './video/video.sass';

import './phones/phones.sass';

import './counters/counters.sass';


import './posts/posts.sass';
import './posts/index';

import './partners/partners.sass';

import './waxom-foot/waxom-foot.sass';




export default () => {};
